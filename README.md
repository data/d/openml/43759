# OpenML dataset: Indian-Liver-Patient-Dataset

https://www.openml.org/d/43759

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Data Set Information
This data set contains 416 liver patient records and 167 non liver patient records.The data set was collected from test samples in North East of Andhra Pradesh, India. 'is_patient' is a class label used to divide into groups(liver patient or not). This data set contains  441 male patient records and 142 female patient records.
Any patient whose age exceeded 89 is listed as being of age "90".
Attribute Information

age Age of the patient
gender Gender of the patient
tot_bilirubin Total Bilirubin
direct_bilirubin Direct Bilirubin
alkphos Alkaline Phosphotase
sgpt Alamine Aminotransferase
sgot Aspartate Aminotransferase
tot_proteins Total Protiens
albumin Albumin
ag_ratio Albumin and Globulin Ratio
is_patient Selector field used to split the data into two sets (labeled by the experts)

Acknowledgements
The data set has been elicit from UCI Machine Learning Repository. My sincere thanks to them.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43759) of an [OpenML dataset](https://www.openml.org/d/43759). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43759/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43759/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43759/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

